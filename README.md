# Calmira Reborn: a Windows 9x-inspired shell for Windows 3.1

![Demonstration](SHOTS/CAL50LFN.BMP)

*Calmira Reborn on Windows for Workgroups 3.11, demonstrating the
Windows 9x-inspired shell, custom window decorations, and support for
Long File Names with the DOSLFN driver running.*

## License

Copyright (C) 2022-2023 Hunter Turcin  
Copyright (C) 2004-2007 Alexandre Rodrigues de Sousa  
Copyright (C) 1998-2002 Calmira Online!  
Copyright (C) 1997-1998 Li-Hsin Huang  

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

## History

This fork of Calmira is fourth in the line of Calmira projects. It descends
from [Calmira LFN 3.32](http://geocities.com/alexanrsousa/calmira.html) by
Alexandre Rodrigues de Sousa, which descends from Calmira Online!'s
[Calmira II 3.3](http://calmira.net), which descends from Li-Hsin Huang's
[Calmira 2.2SR](http://calmira.net/downloads/files/calmir22.zip).

Version history for the parent projects can be found at:

* http://www.geocities.com/alexanrsousa/cal_history.html
* http://calmira.net/
* http://calmira.net/news/oldnews.htm

## Structure

The repository is structured as follows:

* `BMP\`: Raw bitmaps. Calmira Reborn does not look at the contents of this
  directory directly. Updated bitmaps must be manually put into their
  corresponding `.RES` files or `TImage` components.
* `HELP\`: Windows Help-format documentation.
* `SRC\`: GPL2+-licensed source code. This includes several Delphi projects,
  Turbo Pascal source files, resource files, and default configuration.
* `VCL\`: Public domain source code. This includes Delphi components used by
  the projects in the `SRC\` directory.
* `VENDOR\`: Two DLLs required for a release bundle. I do not know the history
  of these files.

## Development

Have the following installed in a 16-bit Windows environment:

* Borland Resource Workshop 4.5
* Calmira Online!'s
  [custom version](http://calmira.net/downloads/files/erwin/isetup16.exe)
  of Inno Setup 1.6
* Delphi 1.02
* Turbo Pascal 7.0 for MS-DOS

In Delphi, register the custom components by going to the Options menu,
selecting Install Components, and navigating to `VCL\CALVCL.PAS`. Then,
navigate to Options -> Environment... -> Library -> Path -> Library Path
and prepend the `VCL` directory followed by a semicolon (`;`). After doing
these, compile each of the seven Delphi projects. While it is possible to
compile `CALMIRA.EXE` without its dependencies, the executable will fail to
run until the other projects are compiled first.

Most `TImage` components don't take a picture directly and instead load the
appropriate bitmap from one of the `RES` files in the `SRC` directory, usually
`BITMAPS.RES`. The method I have been using to update these bitmaps is to
copy the source bitmap in the `BMP` directory using Paintbrush, pasting
the image into Borland Resource Workshop, and saving the modified `RES` file.

There are a few programs that must be compiled with Turbo Pascal:

* `SRC\COUNTTIP.PAS`, used for determine the tip count for `SRC\TIPS.TXT`
* `SRC\DIREXIST.PAS`, used as part of `SRC\START.BAT`
* `SRC\DOSRUN.PAS`, used for running some programs

`SRC\COUNTTIP.PAS` contains a program for counting the number of tips in the
`SRC\TIPS.TXT` file. The first line of this file must contain an integer
with the number of tips reported by the program.

`SRC\DIREXIST.PAS` contains a program that is used by the `START.BAT` file.

I have not attempted to modify or compile the help document yet.

## Releasing

The previous release process appears to have been manual. I have created a
`RELEASE.BAT` script that will, assuming each Delphi project has been compiled,
create a `RELEASE` directory that contains everything needed for running
Calmira Reborn.
